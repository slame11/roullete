<?php

return [

    /*
     * Русская локализация для фраз и слов игры
    */

    'home' => 'Home',
    'how_to_play' => 'How to play?',
    'how_to_play_title' => 'WELCOME!',
    'login' => 'Login',
    'login_button' => 'Login',
    'login_label' => 'Login',
    'password' => 'Password',
    'password_confirm' => 'Verify Password',
    'password_forgot' => 'Forgot password?',
    'dont_have_acc' => 'I do not have an account',
    'logout' => 'Logout',
    'profile' => 'Profile',
    'register' => 'Register',
    'registration' => 'Registration',
    'make_bid' => 'Make a bet',
    'game_info_title' => 'RULES OF THE GAME!',

    'sec' => 'sec',

    'game' => [
        'start' => 'Start game',
        'again' => 'Once again',
        'new' => 'New game',
        'shoot_self' => 'Shoot at yourself',
        'shoot_enemy' => 'Shoot an opponent',
        'roll' => 'Roll drum',
    ],

    'stats' => [
        'played' => 'Games played:',
        'win_sum' => 'Won:',
        'balance' => 'Your balance:',
    ],

    'search' => [
        'title' => 'Wait, we are looking for an opponent',
        'bid' => 'Selected bid:',
    ],

    'found' => [
        'title' => 'Your opponent found!',
        'sub_title' => 'Start the game through:',
    ],

    'bids' => [
        'make_title' => 'Place a bet',
        'make' => 'Make a bet',
        'choose' => 'Select the amount of the bid:',
    ],

    'message' => [
        'winner' => 'Congratulations, you won!',
        'your_win_bid' => 'Your winnings',
        'looser' => 'Sorry, you lost',
        'your_lose_bid' => 'Your loss',
    ],

    'game_rules' => 'Game Rules!',
    'profile_title' => 'GOOD DAY,',
    'profiles' => [
        'title' => 'GOOD DAY,',
        'name' => 'Name',
        'change_character' => 'Change character',
        'phrases' => 'Phrases',
        'your_balance' => 'Your balance',
        'save' => 'Save',
        'withdraw' => 'Withdraw',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirm' => 'Confirm password',
        'refill_balance' => 'Refill balance',
        'refill' => 'Refill',
    ],
    'payment_system' => [
        'choose' => 'Choose the mode of replenishment',
        'sum' => 'Sum of replenishment:',
    ],
];
