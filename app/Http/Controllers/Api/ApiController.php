<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use App\Models\Bid;
use App\Http\Requests\RegisterFormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['winners','lang','getBids']]);
    }

    public function winners(Request $request)
    {
        $winners = User::where('email','!=','admin@admin.com')->limit(5)->inRandomOrder()->get();
        if($winners)
        {
            foreach ($winners as $winner)
            {
                $winner->winsum = $winner->getWinSum();
            }
        }
        else
        {
            $users = User::whereNotNull('id')->limit(5)->inRandomOrder()->get();
            $winners = $users;
        }
        return response()->json(['winners' => $winners], 200);
    }

    public function lang(Request $request) {
        $data = $request->all();
        $lang = Lang::get('game',[],$data['lang']);
        return response($lang);
    }

    public function getBids() {
        $bids = Bid::where('is_active',1)->get();
        return response($bids);
    }

    public function setUserLang(Request $request)
    {
        Auth::user()->lang = $request->get('lang');
        Auth::user()->save();
        return response(Auth::user());
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'string|max:255',
            'email' => 'email||max:255|unique:users,email,'.Auth::id(),
            'password' => 'confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if (isset($data['name']) && Auth::user()->name != $data['name'])
        {
            Auth::user()->name = $data['name'];
        }
        if (isset($data['email']) && Auth::user()->email != $data['email'])
        {
            Auth::user()->email = $data['email'];
        }
        if (isset($data['password']) && $data['password'] == $data['password_confirm'])
        {
            Auth::user()->password = Hash::make($data['password']);
        }
        Auth::user()->save();
        return response(Auth::user());
    }

    public function getProfile(Request $request)
    {
        Auth::user()->lang = $request->get('lang');
        return response(Auth::user());
    }

    public function gameStart(Request $request)
    {
        $data = $request->all();
        $game = Game::where('status',0)->where('bid',$data['bid'])->orderBy('created_at','ASC')->first();
        if($game) {
            $game->player_2 = Auth::id();
            $game->status = 1;
            $game->start_time = now();
        }
        if(!$game) {
            $game = Game::create([
                'bid' => $data['bid'],
                'player_1' => Auth::id(),
                'status' => 0,
            ]);
        }
        $game->save();
        return response($game);
    }
}
