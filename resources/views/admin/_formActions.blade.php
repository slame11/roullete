{{--<form action="{!! $deleteUrl !!}" method="POST">
    <input type="hidden" name="_method" value="delete">
    {{ csrf_field() }}
    <button type="submit" class="btn btn-xs btn-danger">Delete</button>
</form>--}}
<button class="btn btn-xs btn-danger btn-delete" data-remote="{!! $deleteUrl !!}"><i class="glyphicon glyphicon-trash"></i>Delete</button>
<a href="{!! $editUrl !!}" class="btn btn-xs btn-primary" role="button" aria-pressed="true">Edit</a>
<a href="{!! $openUrl !!}" class="btn btn-xs btn-primary" role="button" aria-pressed="true">Open</a>
