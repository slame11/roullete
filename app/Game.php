<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Game extends Model
{
    use CrudTrait;

    protected $table = 'games';

    protected $fillable = ['bid', 'start_time', 'end_time', 'game_time', 'winner', 'player_1', 'player_2', 'status'];
}
