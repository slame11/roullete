<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++) {
            $faker = Faker::create();
            $start = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 years', '+1 month')->getTimestamp());
            $players = [App\User::all()->random()->id, App\User::all()->random()->id];
            $game = \App\Game::create(
                [
                    'bid'      => $faker->randomFloat(0,0,1000),
                    'start_time'       => $start->toDateTimeString(),
                    'end_time'   => $start->addHours( $faker->numberBetween( 1, 8 ) ),
                    'game_time'     => $faker->time(),
                    'winner' => $players[array_rand($players)],
                    'player_1' => $players[0],
                    'player_2' => $players[1],
                    'status' => $faker->numberBetween(1, 5),
                ]
            );
            $log_count = $faker->numberBetween( 1, 8 );
            for($j = 0; $j < $log_count; $j++){
                \App\GameLog::create(
                    [
                        'game_id' => $game->id,
                        'user_id' => $players[array_rand($players)],
                        'action' => 'bid',
                        'time' => $start->addHours( $faker->numberBetween( 1, 8 ) ),
                        'val' => strval($faker->randomFloat(0,0,1000)),
                    ]
                );
            }
        }
    }
}
