<?php

use Illuminate\Database\Seeder;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [

            [
                'name'        => 'Admin',
//                'guard_name'        => 'web, api',
                'permissions' => [
                    'admin',
                    'user'
                ],
            ],
            [
                'name'        => 'Manager',
//                'guard_name'        => 'web, api',
                'permissions' => [
                    'admin',
                    'user'
                ],
            ],
            [
                'name'        => 'User',
//                'guard_name'        => 'web, api',
                'permissions' => [
                    'user'
                ],
            ],
            [
                'name'        => 'Bot',
//                'guard_name'        => 'web, api',
                'permissions' => [
                    'user'
                ],
            ],
        ];

        foreach ($roles as $data) {
            $role        = new \App\Role();
            $role->name  = $data['name'];
//            $role->guard_name  = $data['guard_name'];
            $role->save();

            foreach ($data['permissions'] as $perm) {
                $permission = \Spatie\Permission\Models\Permission::firstOrCreate(
                    ['name' => $perm]
                );
                $role->givePermissionTo($permission);
            }
        }
    }
}
