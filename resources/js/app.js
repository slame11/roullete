import bearer from "@websanova/vue-auth/drivers/auth/bearer";

/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

//import Vue from 'vue';
window.Vue = require('vue');
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import $ from 'jquery';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ChooseAction from './components/ChooseAction'
import Login from './components/Login'
import Register from './components/Register'
import GameMakeBid from './components/GameMakeBid'
import GameRules from './components/GameRules'
import GameInfo from './components/GameInfo'
import GameStart from './components/GameStart'
import Profile from './components/Profile'
import FoundOpponent from './components/FoundOpponent'
import SearchOpponent from './components/SearchOpponent'
import Home from './components/Home'
import Navigation from './components/Navigation'
import MobileNavigation from './components/MobileNavigation'

import GameResult from "../js/components/GameResult";
import Payments from "../js/components/Payments";

// import Login from './components/Login.vue';
// import Home from './components/Home.vue';
// import 'vuetify/dist/vuetify.min.css'

window.Vue.use(VueRouter);
window.Vue.use(Vuetify);
Vue.use(VueAxios, axios);

const jwttoken = localStorage.getItem('laravel-jwt-auth')
if (jwttoken) {
    Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + jwttoken
}

Vue.axios.defaults.baseURL = '/api/';


const routes = [
    {
        path: '/',
        components: { GameStart },
        meta: {
            auth: true
        }
    },
    {
        path: '/login',
        components: { Login },
        meta: {
            auth: false
        }
    },
    {
        path: '/register',
        components: { Register },
        meta: {
            auth: false
        }
    },
    {
        path: '/game/start',
        components: { GameStart },
        meta: {
            auth: true
        }
    },
    {
        path: '/game/make_bid',
        components: { GameMakeBid },
        meta: {
            auth: true
        }
    },
    {
        path: '/game/search',
        components: { SearchOpponent },
        meta: {
            auth: true
        }
    },
    {
        path: '/game/found',
        components: { FoundOpponent },
        meta: {
            auth: true
        }
    },
    {
        path: '/game/action',
        components: { ChooseAction },
        meta: {
            auth: true
        }
    },
];

const router = new VueRouter({ routes });
Vue.router = router

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    tokenDefaultName: 'laravel-jwt-auth',
    tokenStore: ['localStorage'],

    // API endpoints used in Vue Auth.
    registerData: {
        url: 'auth/register',
        method: 'POST',
        redirect: '/login'
    },
    loginData: {
        url: 'auth/login',
        method: 'POST',
        redirect: '/',
        fetchUser: true
    },
    logoutData: {
        url: 'auth/logout',
        method: 'POST',
        redirect: '/',
        makeRequest: true
    },
    fetchData: {
        url: 'auth/user',
        method: 'GET',
        enabled: true
    },
    refreshData: {
        url: 'auth/refresh',
        method: 'GET',
        enabled: true,
        interval: 30
    }
});

window.onload = function () {
    const app = new Vue({
        components: { GameRules, Profile, Login, Register, Navigation, MobileNavigation, GameResult, Payments, GameInfo },
        router,
        beforeMount() {
            this.getLang();
            this.getBids();
        },
        mounted() {
            if (this.isSound) {
                $('#sound').removeClass('active');
            } else {
                $('#sound').addClass('active');
            }
            this.getProfile();

            // if(appv.$auth.check()){
            //
            //     this.$http.post('/user').then(function (resp) {
            //         appv.user = resp.data;
            //         appv.Language.current = resp.data.lang || 'ru';
            //         appv.getLang()
            //     });
            // }
        },
        data() {
            return {
                Language: {
                    img: '/img/buttonlangen.png',
                    current: 'ru',
                    loc: []
                },
                setBid: '',
                bids: [],
                isSound: true,
                isProfileVisible: false,
                isGameRulesVisible: false,
                isGameInfoVisible: false,
                isMobileMenuVisible: false,
                isPaymentsVisible: false,
                currentUser: false,
                currentGame: false,
                allGames: false,
                phrases: 'disabled',
                gameResult: false,
                user: [],
            }
        },
        methods: {
            showProfileModal() {
                this.isProfileVisible = true;
            },
            closeProfileModal() {
                this.isProfileVisible = false;
            },
            showRulesModal() {
                this.isGameRulesVisible = true;
            },
            closeRulesModal() {
                this.isGameRulesVisible = false;
            },
            showGameInfoModal() {
                this.isGameInfoVisible = true;
            },
            closeGameInfoModal() {
                this.isGameInfoVisible = false;
            },
            showMobileMenuModal() {
                this.isMobileMenuVisible = true;
            },
            closeMobileMenuModal() {
                this.isMobileMenuVisible = false;
            },
            showPaymentsModal() {
                this.isPaymentsVisible = true;
                this.isProfileVisible = false;
            },
            closePaymentsModal() {
                this.isPaymentsVisible = false;
                this.isProfileVisible = true;
            },
            toggleFullscreen() {
                if (!document.fullscreenElement &&
                    !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
                    if (document.documentElement.requestFullscreen) {
                        document.documentElement.requestFullscreen();
                    } else if (document.documentElement.msRequestFullscreen) {
                        document.documentElement.msRequestFullscreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullscreen) {
                        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                    $('#full-screen').addClass('active');
                } else {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                    $('#full-screen').removeClass('active');
                }
            },
            toggleSound() {
                if (this.isSound) {
                    $('#sound').removeClass('active');
                    this.isSound = false;
                } else {
                    $('#sound').addClass('active');
                    this.isSound = true;
                }
            },
            toggleLang() {
                let app = this;
                if(app.Language.current === 'ru') {
                    app.Language.current = 'en';
                    app.Language.img = '/img/buttonlangru.png';
                    $('a#lang').children('img.lang-en').hide();
                    $('a#lang').children('img.lang-ru').show();
                }
                else if(app.Language.current === 'en') {
                    app.Language.current = 'ru';
                    app.Language.img = '/img/buttonlangen.png';
                    $('a#lang').children('img.lang-en').show();
                    $('a#lang').children('img.lang-ru').hide();
                }
                app.getLang();
                if(app.$auth.check()){
                    app.setUserLang();
                }
            },
            getLang() {
                let app = this;
                this.$http.post(
                    '/lang',
                    {
                        lang: app.Language.current
                    },
                ).then(function (resp) {
                    app.Language.loc = resp.data;
                });
            },
            getBids() {
                let app = this;
                this.$http.post(
                    '/getBids',
                    {},
                ).then(function (resp) {
                    app.bids = resp.data;
                });
            },
            setUserLang() {
                let app = this;
                this.$http.post(
                    '/setUserLang',
                    {
                        lang: app.Language.current
                    },
                ).then(function (resp) {

                });
            },
            getProfile() {
                let app = this;
                this.$http.post(
                    '/user',
                    {


                    },
                ).then(function (resp) {
                    app.user = resp.data;
                    app.Language.current = app.user.lang || 'ru';
                    app.getLang()
                });
            },
            updateProfile() {
                let app = this;
                this.$http.post(
                    '/profile/update',
                    app.user,
                ).then(function (resp) {

                });
            },
            currentBid(id) {
                let app = this;
                app.setBid = id;
            },
            startGame(){
                let app = this.$root;
                this.$http.post(
                    '/game/start',
                    {
                        bid: app.setBid,
                    },
                ).then(function (resp) {

                });
            }
        },
    }).$mount('#app');
}
