<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('bid');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->time('game_time');
            $table->bigInteger('winner')->unsigned();
            $table->bigInteger('player_1')->unsigned();
            $table->bigInteger('player_2')->unsigned();
            $table->integer('status');
            $table->timestamps();


            $table->foreign('winner')
                ->references('id')
                ->on('users');
            $table->foreign('player_1')
                ->references('id')
                ->on('users');
            $table->foreign('player_2')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
