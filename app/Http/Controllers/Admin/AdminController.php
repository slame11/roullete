<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware(['role:admin']);
    }

    public function index() {
        return view('vendor.backpack.base.dashboard');
    }
}
