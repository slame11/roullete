<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->any('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'namespace' => 'Api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@me');
    Route::post('me', 'AuthController@me');

});
Route::post('winners', 'Api\ApiController@winners');
Route::post('lang', 'Api\ApiController@lang');
Route::post('getBids', 'Api\ApiController@getBids');
Route::post('setUserLang', 'Api\ApiController@setUserLang');
Route::post('profile/get', 'Api\ApiController@getProfile');
Route::post('profile/update', 'Api\ApiController@updateProfile');
Route::post('game/start', 'Api\ApiController@gameStart');
