<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('/games', 'GameController@index')->name('games');
});


Route::post('auth/login', 'Api\AuthController@login');
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'Api\AuthController@user');
});
Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'Api\AuthController@refresh');
});


Route::get('/{any}', 'VueController@index')->where('any', '.*');
