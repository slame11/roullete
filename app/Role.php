<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use \Spatie\Permission\Models\Role as SpateRole;

class Role extends SpateRole
{
    use Notifiable;

	protected $fillable = [
		'name', 'guard_name',
	];
}
