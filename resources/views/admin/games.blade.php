@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    {{--<h1>Игры</h1>--}}
@stop

@section('content')

    <games-component :all-games="{{ json_encode($games) }}"></games-component>

@stop
