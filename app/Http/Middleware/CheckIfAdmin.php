<?php

namespace App\Http\Middleware;

use Backpack\Base\app\Http\Middleware\CheckIfAdmin as CheckAdmin;

class CheckIfAdmin extends CheckAdmin
{
}
