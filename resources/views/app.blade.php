<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <meta name="csrf-token" content="{{csrf_token()}}">

        <script src="{{ asset('js/app.js') }}"></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="app" id="app">
            <profile v-show="isProfileVisible" @close="closeProfileModal"></profile>
            <payments v-show="isPaymentsVisible" @close="closePaymentsModal"></payments>
            <game-rules v-show="isGameRulesVisible" @close="closeRulesModal" ></game-rules>
            <game-info v-show="isGameInfoVisible" @close="closeGameInfoModal" ></game-info>
            <navigation></navigation>
            <mobile-navigation v-show="isMobileMenuVisible" @close="closeMobileMenuModal"></mobile-navigation>
            <game-result v-bind:game-result="gameResult" v-show="gameResult"></game-result>
            <a class="nav-mobile-button" @click="showMobileMenuModal">Меню</a>
            <div id="saloon">
            </div>
            <div id="game">
                <div id="title"><img src="/img/game-title.png"></div>
                <div id="buttons">
                    <a id="info" @click="showGameInfoModal"></a>
                    <a id="sound" @click="toggleSound"></a>
                    <a id="lang" @click="toggleLang">
                        <img class="lang-en" src="/img/buttonlangen.png">
                        <img class="lang-ru" style="display: none" src="/img/buttonlangru.png">
                    </a>
                    <a id="full-screen" @click="toggleFullscreen()"></a>
                </div>
                <div id="bottom-game-panel">
                    <img id="bottom-game-avatar" src="/img/avatar-default.png">
                    <div id="bottom-game-avatar-bg"></div>
                </div>
                <router-view name="Login"></router-view>
                <router-view name="Register"></router-view>
                <router-view name="GameStart"></router-view>
                <router-view name="GameMakeBid"></router-view>
                <router-view name="SearchOpponent"></router-view>
                <router-view name="FoundOpponent"></router-view>
                <router-view name="ChooseAction"></router-view>
            </div>
            <div id="bgelements"></div>

            <div class="animations">
                <div class="pistol_smoke"></div>
                <div class="bank_smoke"></div>
                <div class="right_trash"></div>
                <div class="bird"></div>
            </div>
        </div>
    </body>
    <script>
        {{--window.auth_user = @php(json_encode($auth_user))--}}
        {{--console.log(window.auth_user);--}}
    </script>
</html>
<script>
</script>
