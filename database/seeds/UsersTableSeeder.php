<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email'      => 'admin@admin.com',
                'name'       => 'Admin',
                'password'   => '111111',
                'phone'     => '+1234567890123',
                'person_key' => \Webpatser\Uuid\Uuid::generate()->string,
                'address' => '',
                'lang' => 'ru',
                'balance' => 3900.00,
                'roles'      => [
                    'Admin',
                ],
            ],
        ];

        foreach ($users as $data) {
            $user = \App\User::create(
                [
                    'email'      => $data['email'],
                    'name'       => $data['name'],
                    'password'   => \Illuminate\Support\Facades\Hash::make($data['password']),
                    'phone'   => $data['phone'],
                    'email_verified_at' => now(),
                    'person_key'   => $data['person_key'],
                    'address'   => $data['address'],
                    'balance'   => $data['balance'],
                    'lang'   => $data['lang'],

                ]
            );
            foreach ($data['roles'] as $r) {
                $role = \App\Role::where(['name' => $r])->first();
                $user->assignRole($role);
            }
        }


        $first_name = ["Прохор","Харитон","Давид","Людовик","Зураб","Иван","Петр","Артур","Устин","Виктор","Витя","Ефим","Ждан"];
        $second_name = ["Егоров","Иванов","Воробьёв","Петров","Ефименко","Матвеев","Савельев","Васильев","Королёв","Кулишенко","Кравчук","Блинов"];

        for ($i = 1; $i < 100; $i++) {
            $faker = Faker::create();
            $fName = array_rand($first_name);
            $sName = array_rand($second_name);
            $user = \App\User::create(
                [
                    'email'      => $faker->email,
                    'name'       => $first_name[$fName] . ' ' . $second_name[$sName],
                    'password'   => \Illuminate\Support\Facades\Hash::make($data['password']),
                    'phone'     => '+1234567890123',
                    'person_key' => \Webpatser\Uuid\Uuid::generate()->string,
                    'address' => '',
                    'lang' => 'ru',
                    'balance' => 3900.00,
                ]
            );

            $data['roles'] = ['Bot'];

            foreach ($data['roles'] as $r) {
                $role = \App\Role::where(['name' => $r])->first();
                $user->assignRole($role);
            }
        }
    }
}

