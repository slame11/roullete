@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Игра #{{ $game->id }}</h1>
@stop

@section('content')

    TODO: gameinfo

    <table class="datatables table-bordered table-hover" id="gamelogs-table" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Action</th>
                <th>Value</th>
                <th>Time</th>
            </tr>
        </thead>
    </table>
@stop

@section('adminlte_js')
    <script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script>
        function htmlDecode(data){
            var txt=document.createElement('textarea');
            txt.innerHTML=data;
            return txt.value
        }
        $(function () {
            $('#gamelogs-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{ route('admin.gamelogs_datatable', $game->id) }}',
                "columns": [
                    { "data": "id" },
                    { "data": "user_name" },
                    { "data": "action" },
                    { "data": "val" },
                    { "data": "time" },
                ]
            });

            $('#gamelogs-table tbody').on('click','tr td:not(:last-child)', function () {
                console.log('clicked');
            });

        });

    </script>
@stop
